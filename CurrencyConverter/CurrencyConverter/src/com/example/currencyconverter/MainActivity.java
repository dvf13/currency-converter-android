package com.example.currencyconverter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity {

	private Spinner sCurrencies1, sCurrencies2;
	private TextView tValueCurrency1, tValueCurrency2;
	private Button bConvert, bClear;
	private TextView mTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		sCurrencies1 = (Spinner) findViewById(R.id.spinner_curr1);
		sCurrencies2 = (Spinner) findViewById(R.id.spinner_curr2);
		tValueCurrency1 = (TextView) findViewById(R.id.editText_curr1);
		tValueCurrency2 = (TextView) findViewById(R.id.editText_curr2);
		bConvert = (Button) findViewById(R.id.button_convert);
		bClear = (Button) findViewById(R.id.button_clear);
		mTextView = (TextView) findViewById(R.id.textView1);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.currencies_array,
				android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		sCurrencies1.setAdapter(adapter);
		sCurrencies2.setAdapter(adapter);

		bConvert.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					tValueCurrency2.setText(String.valueOf(convertCurrencies()));
					mTextView.setText("Exchange rate of "
							+ convertResponseToStringArray()[4] + " on "
							+ convertResponseToStringArray()[3].replace("\"", "") + " at "
							+ convertResponseToStringArray()[5].replace("\"", ""));
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		bClear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tValueCurrency1.setText("");
				tValueCurrency2.setText("");
				mTextView.setText("");
			}
		});

	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("RATE", mTextView.getText().toString());
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		mTextView.setText(savedInstanceState.getString("RATE"));
	}

	public String getsCurrencies1() {
		return sCurrencies1.getSelectedItem().toString();
	}

	public String getsCurrencies2() {
		return sCurrencies2.getSelectedItem().toString();
	}

	public String gettValueCurrency1() {
		return tValueCurrency1.getText().toString();
	}

	public void settValueCurrency1(TextView tValueCurrency1) {
		this.tValueCurrency1 = tValueCurrency1;
	}

	public String getTextInBrackets(String text) {
		char[] arr = text.toCharArray();
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == '(') {
				while (arr[i + 1] != ')') {
					str.append(arr[i + 1]);
					i++;
				}
			}
		}
		return str.toString();
	}

	public String doGet() throws IllegalStateException, IOException {
		HttpResponse response = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet req = new HttpGet(
					"http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sabd1l1t1&s="
							+ getTextInBrackets(getsCurrencies1())
							+ getTextInBrackets(getsCurrencies2()) + "=X");
			response = client.execute(req);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		return result.toString();
	}

	public String[] convertResponseToStringArray()
			throws IllegalStateException, IOException {
		String[] resp = doGet().split(",");
		return resp;
	}

	public double convertCurrencies() throws IllegalStateException, IOException {
		double value1 = Double.parseDouble(gettValueCurrency1());
		String[] resp = doGet().split(",");
		double rate = Double.parseDouble(resp[4]);
		return value1 * rate;
	}

}
